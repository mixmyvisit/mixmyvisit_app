import 'dart:convert';
import 'dart:math';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:digital_souvenir/api/exceptions.dart';

import 'package:digital_souvenir/models/LocationInMixmvModel.dart';
import 'package:digital_souvenir/models/LocationListInMixmvModel.dart';

class MmvService {
  MmvService(this._dio);
  final Dio _dio;
  final mmvApiKey = env['MMV_API_KEY'];

  Future<bool> cancelVisit(tagId) async {
    try {
      await _dio.post(
        'https://mixmyvisit.web.ua.pt/api/mixmvs/cancel',
        options: Options(
          headers: {
            Headers.contentTypeHeader: 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $mmvApiKey'
          },
        ),
        data: {'tagID': tagId},
      );

      return true;
    } on DioError catch (dioError) {
      print(dioError);
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<bool> createTokenToEndVisit(tagID) async {
    try {
      await _dio
          .post('https://mixmyvisit.web.ua.pt/api/mixmvs/create-redirect-token',
              options: Options(
                headers: {
                  Headers.contentTypeHeader: 'application/json; charset=UTF-8',
                  'Authorization': 'Bearer $mmvApiKey'
                },
              ),
              data: {
            'tagID': tagID,
          });
      return true;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<bool> startEndProcessOfPersVisit(
      tagID, userID, locationsLength) async {
    try {
      await _dio.post(
        'https://mixmyvisit.web.ua.pt/api/mixmvs/render/user',
        options: Options(
          headers: {
            Headers.contentTypeHeader: 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $mmvApiKey'
          },
        ),
        data: jsonEncode(<String, dynamic>{
          'tagID': tagID,
          'userID': userID,
          'videosDuration': locationsLength
        }),
      );
      return true;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<bool> startEndProcessOfNonPersVisit(tagID, locationsLength) async {
    try {
      await _dio.post(
        'https://mixmyvisit.web.ua.pt/api/mixmvs/render/non-user',
        options: Options(
          headers: {
            Headers.contentTypeHeader: 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $mmvApiKey'
          },
        ),
        data: jsonEncode(<String, dynamic>{
          'tagID': tagID,
          'videosDuration': locationsLength
        }),
      );
      return true;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<bool> createSimpleVisit(tagId) async {
    try {
      await _dio.post(
        'https://mixmyvisit.web.ua.pt/api/mixmvs/create/non-user',
        options: Options(
          headers: {
            Headers.contentTypeHeader: 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $mmvApiKey'
          },
        ),
        data: jsonEncode(<String, String>{
          'tagID': tagId,
        }),
      );

      //final getMessage = res.data;
      //return getMessage['message'];
      return true;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<Map> generateAndAddPin(tagID) async {
    String generatePin;
    List<String> generatePinList = [];

    for (var i = 0; i < 4; i++) {
      Random random = new Random();
      int min = 1;
      int max = 10;

      int randomNumber = min + random.nextInt(max - min);
      generatePinList.add(randomNumber.toString());
    }

    generatePin = generatePinList.join();

    try {
      await _dio.post('https://mixmyvisit.web.ua.pt/api/messenger-pins/store',
          data:
              jsonEncode(<String, String>{'tagID': tagID, 'pin': generatePin}),
          options: Options(
            headers: {
              Headers.contentTypeHeader: 'application/json; charset=UTF-8',
              'Authorization': 'Bearer $mmvApiKey' // set content-length
            },
          ));

      return {'pinList': generatePinList, 'pinString': generatePin};
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<Map<String, bool>> verifyTagRegistration(tagId, changeState) async {
    try {
      final requestVerifyTagRegistration = await _dio.post(
        'https://mixmyvisit.web.ua.pt/api/mixmvs/verify-check-visit-type',
        data: {'tagID': tagId},
        options: Options(
          headers: {
            Headers.contentTypeHeader: 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $mmvApiKey'
          },
        ),
      );

      var requestData = requestVerifyTagRegistration.data;

      if (requestData['visit'] != null) {
        if (requestData['user'] != null) {
          changeState({
            'tagIdIsRegistered': true,
            'tagIsAssociatedWithVisit': true,
            'user': requestData['user'],
            'visit': requestData['visit']
          });
        } else {
          changeState({
            'tagIdIsRegistered': true,
            'tagIsAssociatedWithVisit': true,
            'visit': requestData['visit']
          });
        }

        if (requestData['visit']['states_id'] == 2 ||
            requestData['visit']['states_id'] == 8 ||
            requestData['visit']['states_id'] == 14) {
          return {"isVisitRendering": true, "isAssociatedWithVisit": true};
        } else {
          return {"isVisitRendering": false, "isAssociatedWithVisit": true};
        }
      } else {
        changeState({
          'tagIdIsRegistered': false,
          'tagIsAssociatedWithVisit': false,
        });

        return {"isVisitRendering": false, "isAssociatedWithVisit": false};
      }
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<bool> addLocationToVisit(tagID, locationName) async {
    try {
      await _dio.post('https://mixmyvisit.web.ua.pt/api/mixmvs/add-location',
          options: Options(
            headers: {
              Headers.contentTypeHeader: 'application/json; charset=UTF-8',
              'Authorization': 'Bearer $mmvApiKey'
            },
          ),
          queryParameters: {
            'tagID': tagID,
            'locationName': locationName,
          });
      return true;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<LocationListInMixmvModel> getVisitLocations(tagID) async {
    try {
      final res = await _dio
          .post('https://mixmyvisit.web.ua.pt/api/mixmvs/tag-locations',
              options: Options(
                headers: {
                  Headers.contentTypeHeader: 'application/json; charset=UTF-8',
                  'Authorization': 'Bearer $mmvApiKey'
                },
              ),
              queryParameters: {
            'tagID': tagID,
          });

      LocationListInMixmvModel locationsList =
          LocationListInMixmvModel.fromJson(res.data['data']);

      return locationsList;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }
}
