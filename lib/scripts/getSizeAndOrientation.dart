import 'package:flutter/material.dart';

Map getSizeAndOrientation(BuildContext context) {
  Map sizeAndOrientation = {
    'width': MediaQuery.of(context).size.width,
    'orientation': MediaQuery.of(context).orientation
  };

  return sizeAndOrientation;
}
