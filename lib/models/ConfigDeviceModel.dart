class ConfigDevice {
  final String deviceId;
  final String facebookAppTagAssociationUrl;
  final String previewUrl;
  final String editUrl;
  final int qrCodeDurationOnScreenInSeconds;

  ConfigDevice(
      {this.deviceId,
      this.facebookAppTagAssociationUrl,
      this.previewUrl,
      this.editUrl,
      this.qrCodeDurationOnScreenInSeconds});

  factory ConfigDevice.fromJson(Map<String, dynamic> json) {
    return ConfigDevice(
      deviceId: json['deviceId'],
      facebookAppTagAssociationUrl: json['facebookAppTagAssociationUrl'],
      previewUrl: json['previewUrl'],
      editUrl: json['editUrl'],
      qrCodeDurationOnScreenInSeconds: json['QrCodeDurationOnScreenInSeconds'],
    );
  }
}
