import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:digital_souvenir/providers/mmvServiceProvider.dart';
import 'package:digital_souvenir/providers/alticeServiceProvider.dart';
import 'package:digital_souvenir/api/exceptions.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:digital_souvenir/components/FeedbackScreen.dart';
import 'package:digital_souvenir/components/Loader.dart';
import 'package:digital_souvenir/providers/state.dart';

final createSimpleVisitFuture = FutureProvider.autoDispose<bool>((ref) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final mmvService = ref.read(mmvServiceProvider);
  final alticeService = ref.read(alticeServiceProvider);

  await alticeService.registerTagId(tagId);
  return await mmvService.createSimpleVisit(tagId);
});

class StartSimpleVisit extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: MmvAppBar(),
          body: BodyBase(
              childWidget: watch(createSimpleVisitFuture).when(
                  data: (_) {
                    return FeedbackScreen(
                        feedbackType: true,
                        duration: 20,
                        messageWidget: [
                          Container(
                            padding: EdgeInsets.only(bottom: 4.0.h),
                            child: Text(
                                'A sua visita simples foi criada com sucesso!',
                                style: Theme.of(context).textTheme.headline1),
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 1.0.h),
                            child: Text(
                                'Pode agora iniciar o percurso da sua visita podendo adicionar localizações à mesma quando passar a sua pulseira nos dispositivos dessas mesmas localizações.',
                                style: Theme.of(context).textTheme.bodyText1),
                          ),
                          Text(
                              'Caso pretenda terminar ou cancelar a sua visita também terá essas funcionalidades sempre disponiveis após passar a sua pulseira NFC neste dispostivo.',
                              style: Theme.of(context).textTheme.bodyText1)
                        ]);
                  },
                  error: (e, s) {
                    if (e is ApiRequestException) {
                      return FeedbackScreen(messageWidget: [
                        Text(e.message,
                            style: Theme.of(context).textTheme.bodyText1)
                      ], feedbackType: false);
                    } else {
                      return FeedbackScreen(messageWidget: [
                        Text(
                            'Pedimos desculpa mas ocorreu um erro na criação da sua visita simples. Tente novamente mais tarde.',
                            style: Theme.of(context).textTheme.bodyText1)
                      ], feedbackType: false);
                    }
                  },
                  loading: () => LoaderWithMessage(
                      message:
                          'Por favor aguarde enquanto criamos a sua visita simples'))),
        ));
  }
}
