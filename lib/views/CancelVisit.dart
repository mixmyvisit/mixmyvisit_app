import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:digital_souvenir/providers/mmvServiceProvider.dart';
import 'package:digital_souvenir/api/exceptions.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:digital_souvenir/components/Loader.dart';
import 'package:digital_souvenir/components/FeedbackScreen.dart';
import 'package:digital_souvenir/providers/state.dart';

final cancelVisitFuture = FutureProvider.autoDispose<bool>((ref) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final mmvService = ref.read(mmvServiceProvider);
  final cancelVisitRequest = await mmvService.cancelVisit(tagId);
  return cancelVisitRequest;
});

class CancelVisit extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final doesTagIdHasUser = watch(globalStateProvider.state)['user'];
    final visitType = doesTagIdHasUser == '' ? 'simples' : 'personalizada';

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          appBar: MmvAppBar(),
          body: BodyBase(
              childWidget: watch(cancelVisitFuture).when(
                  data: (_) {
                    return FeedbackScreen(feedbackType: true, messageWidget: [
                      Container(
                        padding: EdgeInsets.only(bottom: 4.0.h),
                        child: Text(
                            'A sua visita $visitType foi cancelada com sucesso',
                            style: Theme.of(context).textTheme.headline1),
                      ),
                      Container(
                          padding: EdgeInsets.only(bottom: 1.0.h),
                          child: Text(
                              'Esperemos que tenha se divertido ao experienciar uma visita com o MixMyVisit.',
                              style: Theme.of(context).textTheme.bodyText1)),
                      Text(
                          'Caso pretenda fornecer-nos feedback acerca da nossa aplicação e no que poderiamos melhor contacte-nos via email: mixmmv.ua@gmail.com',
                          style: Theme.of(context).textTheme.bodyText1)
                    ]);
                  },
                  error: (e, s) {
                    if (e is ApiRequestException) {
                      return FeedbackScreen(messageWidget: [
                        Text(e.message,
                            style: Theme.of(context).textTheme.bodyText1)
                      ], feedbackType: false);
                    } else {
                      return FeedbackScreen(messageWidget: [
                        Text("Oops, parece que algo correu mal...",
                            style: Theme.of(context).textTheme.bodyText1)
                      ], feedbackType: false);
                    }
                  },
                  loading: () => LoaderWithMessage(
                      message:
                          'Por favor aguarde enquanto cancelamos e apagamos a sua visita')))),
    );
  }
}
