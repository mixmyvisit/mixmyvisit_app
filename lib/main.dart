import 'package:digital_souvenir/views/CancelVisit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:navigation_history_observer/navigation_history_observer.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

import 'package:digital_souvenir/theme/styles.dart';
import 'package:digital_souvenir/views/Home.dart';
import 'package:digital_souvenir/views/TagMenu.dart';
import 'package:digital_souvenir/views/StartPersonalizedVisit.dart';
import 'package:digital_souvenir/views/StartSimpleVisit.dart';
import 'package:digital_souvenir/views/EndVisit.dart';
import 'package:digital_souvenir/views/RedirectToEditor.dart';
import 'package:digital_souvenir/views/RedirectToBot.dart';
import 'package:digital_souvenir/views/RegisterLocation.dart';
import 'package:digital_souvenir/views/RegisteredLocationsInfo.dart';
import 'package:digital_souvenir/components/SplashScreen.dart';

Future main() async {
  await DotEnv.load(fileName: ".env");
  runApp(
    ProviderScope(child: App()),
  );
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizerUtil().init(constraints, orientation);
        return MaterialApp(
          title: 'MixMyVisit',
          home: SplashScreen(),
          theme: appTheme(),
          routes: {
            '/home': (_) => Home(),
            '/tag-menu': (_) => TagMenu(),
            '/start-personalized-visit': (_) => StartPersonalizedVisit(),
            '/start-simple-visit': (_) => StartSimpleVisit(),
            '/cancel-visit': (_) => CancelVisit(),
            '/end-visit': (_) => EndVisit(),
            '/redirect-to-bot': (_) => RedirectToBot(),
            '/redirect-to-editor': (_) => RedirectToEditor(),
            '/register-location': (_) => RegisterLocation(),
            '/registered-locations-info': (_) => RegisteredLocationsInfo(),
          },
          navigatorObservers: [NavigationHistoryObserver()],
        );
      });
    });
  }
}
