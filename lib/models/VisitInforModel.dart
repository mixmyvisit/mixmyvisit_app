class VisitInfo {
  final String id;
  final String name;
  final String email;
  final String contact;
  final String userStatus;
  final String tagNumber;
  final int scans;
  final String tagCode;

  VisitInfo(
      {this.id,
      this.name,
      this.email,
      this.contact,
      this.userStatus,
      this.tagNumber,
      this.scans,
      this.tagCode});

  factory VisitInfo.fromJson(Map<String, dynamic> json) {
    return VisitInfo(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      contact: json['contact'],
      userStatus: json['userStatus'],
      tagNumber: json['tagNumber'],
      scans: json['scans'],
      tagCode: json['tagCode'],
    );
  }
}
