class LocationInTagModel {
  final String id;
  final String name;
  final String latitude;
  final String longitude;
  final List<dynamic> timestamp;
  final List<dynamic> contents;

  LocationInTagModel(
      {this.id,
      this.name,
      this.latitude,
      this.longitude,
      this.timestamp,
      this.contents});

  factory LocationInTagModel.fromJson(Map<String, dynamic> json) {
    return LocationInTagModel(
        id: json['id'],
        name: json['name'],
        latitude: json['latitude'],
        longitude: json['longitude'],
        timestamp: json['timestamp'],
        contents: json['contents']);
  }
}
