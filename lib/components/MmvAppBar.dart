import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';

class MmvAppBar extends AppBar {
  MmvAppBar({Key key, bool showGoBack = false})
      : super(
            key: key,
            backwardsCompatibility: false,
            systemOverlayStyle: SystemUiOverlayStyle.light,
            centerTitle: true,
            titleSpacing: 50,
            elevation: 10,
            toolbarHeight: 10.0.h,
            automaticallyImplyLeading: showGoBack,
            brightness: Brightness.light,
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                'assets/logo/mmv-logo-white.png',
                height: 8.0.h,
              ),
            ));
}
