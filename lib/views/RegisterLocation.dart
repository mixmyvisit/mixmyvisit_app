import 'package:digital_souvenir/api/exceptions.dart';
import 'package:digital_souvenir/providers/alticeServiceProvider.dart';
import 'package:digital_souvenir/providers/mmvServiceProvider.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:responsive_ui/responsive_ui.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';

import 'package:digital_souvenir/components/FeedbackScreen.dart';
import 'package:digital_souvenir/components/Loader.dart';
import 'package:digital_souvenir/models/LocationModel.dart';
import 'package:digital_souvenir/models/LocationListModel.dart';
import 'package:digital_souvenir/providers/state.dart';

final registerLocationFuture =
    FutureProvider.autoDispose.family<bool, Location>((ref, location) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final alticeService = ref.read(alticeServiceProvider);
  final mmvService = ref.read(mmvServiceProvider);

  await alticeService.registerLocation(tagId, location.id);
  return await mmvService.addLocationToVisit(tagId, location.name);
});

final getAllLocationsFuture =
    FutureProvider.autoDispose<LocationList>((ref) async {
  ref.maintainState = true;
  final alticeService = ref.read(alticeServiceProvider);

  return await alticeService.getAllLocations();
});

class RegisterLocation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterLocationState();
}

class RegisterLocationState extends State<RegisterLocation> {
  List locations;
  bool didSaveLocation = false;
  Location selectedLocation;
  String selectedLocationId;
  String selectedLocationName;
  String selectedLocationLatitude;
  String selectedLocationLongitude;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => didSaveLocation == false,
        child: Scaffold(
          appBar: MmvAppBar(
            showGoBack: didSaveLocation == false,
          ),
          body: BodyBase(
            childWidget: Builder(builder: (BuildContext context) {
              if (didSaveLocation == false) {
                return Consumer(
                  builder: (context, watch, child) {
                    return watch(getAllLocationsFuture).when(
                        data: (data) {
                          return Flex(
                              direction: Axis.vertical,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    padding: EdgeInsets.only(bottom: 4.0.h),
                                    child: Text('Selecione a localização',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1)),
                                Container(
                                    padding: EdgeInsets.only(bottom: 3.0.h),
                                    child: Text(
                                        'Para concluir a adição da localização à sua visita e pulseira necessita de selecionar a localização pretendida e carregar no botão de "Adicionar localização" que irá surgir abaixo.',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1)),
                                Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      value: selectedLocationId,
                                      dropdownColor: Colors.grey[300],
                                      isExpanded: true,
                                      icon: Container(
                                          padding: EdgeInsets.only(left: 2),
                                          child: Icon(Icons.arrow_drop_down,
                                              size: 30)),
                                      hint: Text('Escolha uma localização',
                                          style: TextStyle(
                                              color: Colors.grey[500])),
                                      iconSize: 18,
                                      elevation: 8,
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.w300),
                                      onChanged: (String newValue) {
                                        final findLocation = data.locations
                                            .firstWhere((location) =>
                                                location.id == newValue);

                                        setState(() {
                                          selectedLocation = findLocation;
                                          selectedLocationId = findLocation.id;
                                          selectedLocationName =
                                              findLocation.name;
                                          selectedLocationLatitude =
                                              findLocation.latitude;
                                          selectedLocationLongitude =
                                              findLocation.longitude;
                                        });
                                      },
                                      items: data.locations
                                          .map<DropdownMenuItem<String>>(
                                              (Location locationInfo) {
                                        return DropdownMenuItem<String>(
                                          value: locationInfo.id,
                                          child: Text(locationInfo.name,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                                Builder(builder: (BuildContext context) {
                                  if (selectedLocationId != null) {
                                    String imageName = selectedLocationName
                                        .replaceAll(' ', '-')
                                        .toLowerCase();

                                    return Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 6.0.h, 0, 4.0.h),
                                              child: Text(selectedLocationName,
                                                  textAlign: TextAlign.center,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline1)),
                                          Responsive(
                                              crossAxisAlignment:
                                                  WrapCrossAlignment.center,
                                              runSpacing: 4.0.h,
                                              children: [
                                                Div(
                                                  colS: 12,
                                                  colM: 6,
                                                  child: Container(
                                                      padding: EdgeInsets.only(
                                                          bottom: 4.0.h),
                                                      child: Center(
                                                          child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8.0),
                                                        child: Image.asset(
                                                          'assets/images/$imageName.png',
                                                          fit: BoxFit.contain,
                                                          width: 100.0.w,
                                                        ),
                                                      ))),
                                                ),
                                                Div(
                                                    colS: 12,
                                                    offsetM: 1,
                                                    colM: 5,
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    bottom:
                                                                        4.0.h),
                                                            child: Text(
                                                                '$selectedLocationLatitude | $selectedLocationLongitude')),
                                                        Center(
                                                            child: ElevatedButton(
                                                                style: ElevatedButton.styleFrom(
                                                                    padding: EdgeInsets.symmetric(horizontal: 4.0.h, vertical: 3.0.h),
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              30.0),
                                                                    )),
                                                                onPressed: () {
                                                                  setState(() {
                                                                    didSaveLocation =
                                                                        true;
                                                                  });
                                                                },
                                                                child: Text('Adicionar localização')))
                                                      ],
                                                    ))
                                              ]),
                                        ]);
                                  } else {
                                    return Container(
                                      width: 0.0,
                                      height: 0.0,
                                    );
                                  }
                                })
                              ]);
                        },
                        error: (e, s) {
                          if (e is ApiRequestException) {
                            return FeedbackScreen(messageWidget: [
                              Text(e.message,
                                  style: Theme.of(context).textTheme.bodyText1)
                            ], feedbackType: false);
                          } else {
                            return FeedbackScreen(messageWidget: [
                              Text(
                                  'Pedimos desculpa mas ocorreu um erro na obtenção das localizações disponíveis para adicionar à sua visita. Tente novamente mais tarde.',
                                  style: Theme.of(context).textTheme.bodyText1)
                            ], feedbackType: false);
                          }
                        },
                        loading: () => LoaderWithMessage(
                            message:
                                'Por favor aguarde enquanto obtemos todas as localizações disponíveis'));
                  },
                );
              } else {
                return Consumer(
                  builder: (context, watch, child) {
                    final registerLocation =
                        watch(registerLocationFuture(selectedLocation));

                    return registerLocation.when(
                      data: (_) {
                        return FeedbackScreen(messageWidget: [
                          Container(
                              padding: EdgeInsets.only(bottom: 4.0.h),
                              child: Text(
                                  'A localização $selectedLocationName foi adicionada com sucesso à sua visita!',
                                  style:
                                      Theme.of(context).textTheme.headline1)),
                          Container(
                            padding: EdgeInsets.only(bottom: 1.0.h),
                            child: Text(
                                'Pode agora continuar o percurso da sua visita podendo adicionar ainda mais localizações à mesma.',
                                style: Theme.of(context).textTheme.bodyText1),
                          ),
                          Text(
                              'Caso pretenda terminar ou cancelar a sua visita pode sempre fazê-lo após passar a sua pulseira NFC neste dispostivo ou noutros dispositivos que estejam no percurso da visita.',
                              style: Theme.of(context).textTheme.bodyText1)
                        ], feedbackType: true);
                      },
                      error: (e, s) {
                        if (e is ApiRequestException) {
                          return FeedbackScreen(messageWidget: [
                            Text(e.message,
                                style: Theme.of(context).textTheme.bodyText1)
                          ], feedbackType: false);
                        } else {
                          return FeedbackScreen(messageWidget: [
                            Text(
                                'Pedimos desculpa mas ocorreu um erro ao adicionar a localização à sua visita. Tente novamente mais tarde.',
                                style: Theme.of(context).textTheme.bodyText1)
                          ], feedbackType: false);
                        }
                      },
                      loading: () => LoaderWithMessage(
                          message:
                              'Por favor aguarde enquanto registamos a localização - $selectedLocationName - à sua visita'),
                    );
                  },
                );
              }
            }),
          ),
        ));
  }
}
