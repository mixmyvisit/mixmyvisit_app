import 'package:digital_souvenir/models/ConfigDeviceModel.dart';

class ConfigList {
  final List<ConfigDevice> config;

  ConfigList({
    this.config,
  });

  factory ConfigList.fromJson(List<dynamic> parsedJson) {
    List<ConfigDevice> config = [];
    config = parsedJson.map((i) => ConfigDevice.fromJson(i)).toList();

    return new ConfigList(config: config);
  }
}
