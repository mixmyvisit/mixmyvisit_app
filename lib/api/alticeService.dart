import 'dart:io';
import 'package:dio/dio.dart';
import 'package:device_info/device_info.dart';

import 'package:digital_souvenir/api/exceptions.dart';
import 'package:digital_souvenir/models/LocationListInTagModel.dart';
import 'package:digital_souvenir/models/LocationListModel.dart';

class AlticeService {
  AlticeService(this._dio);
  final Dio _dio;

  Future<bool> registerTagId(tagId) async {
    try {
      await _dio.get(
          'https://meoapp.ptinovacao.pt/2020/Dev/DigitalSouvenirWebApi/api/register',
          queryParameters: {'tagExternalId': tagId});
      return true;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<LocationListInTagModel> getTagLocations(tagId) async {
    try {
      final res = await _dio.get(
        'https://meoapp.ptinovacao.pt/2020/Dev/DigitalSouvenirWebApi/api/contents',
        queryParameters: {'tagExternalId': '$tagId'},
      );
      LocationListInTagModel locationList =
          LocationListInTagModel.fromJson(res.data);

      return locationList;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<bool> registerLocation(tagId, locationId) async {
    var deviceInfo = DeviceInfoPlugin();
    var deviceId;
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      deviceId = iosDeviceInfo.identifierForVendor;
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      deviceId = androidDeviceInfo.androidId;
    }

    try {
      await Future.wait([
        _dio.get(
            'https://meoapp.ptinovacao.pt/2020/Dev/DigitalSouvenirWebApi/api/deviceRegister',
            queryParameters: {'locationId': locationId, 'deviceId': deviceId}),
        _dio.get(
            'https://meoapp.ptinovacao.pt/2020/Dev/DigitalSouvenirWebApi/api/tagvisit',
            queryParameters: {
              'tagExternalId': tagId,
              'locationId': locationId
            }),
      ]);

      return true;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }

  Future<LocationList> getAllLocations() async {
    try {
      final requestAllLocations = await _dio.get(
          'https://meoapp.ptinovacao.pt/2020/Dev/DigitalSouvenirWebApi/api/locations',
          queryParameters: {'type': 'all'});

      LocationList locationList =
          LocationList.fromJson(requestAllLocations.data);

      return locationList;
    } on DioError catch (dioError) {
      throw ApiRequestException.fromDioError(dioError);
    }
  }
}
