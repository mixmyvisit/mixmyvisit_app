import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:digital_souvenir/api/mmvService.dart';

final mmvServiceProvider = Provider<MmvService>((ref) {
  return MmvService(Dio());
});
