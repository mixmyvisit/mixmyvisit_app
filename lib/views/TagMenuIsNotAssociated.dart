import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:responsive_ui/responsive_ui.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:digital_souvenir/components/Dialogs.dart';

class TagMenuIsNotAssociated extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var width = MediaQuery.of(context).size.width;

    double buttonSize = orientation == Orientation.landscape || width > 600
        ? 270.0
        : double.infinity;

    return Center(
        child: Column(children: [
      Responsive(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: <Widget>[
          Div(
            colS: 12,
            colM: 6,
            child: Container(
                alignment: Alignment(0, 0),
                padding: orientation == Orientation.landscape || width > 600
                    ? EdgeInsets.only(bottom: 1.5.h)
                    : EdgeInsets.only(bottom: 2.0.h),
                child: SvgPicture.asset(
                    'assets/illustrations/wristband-not-associated.svg',
                    height: 35.0.h,
                    semanticsLabel:
                        'NFC wristband not associated with a visit illustration')),
          ),
          Div(
              colS: 12,
              colM: 6,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 3.0.h),
                    child: Text(
                        'Verificamos que a pulseira NFC que possuí ainda não está associada a uma visita',
                        style: Theme.of(context).textTheme.headline1),
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 3.0.h),
                      child: ListBody(children: [
                        Text(
                            'Pode começar uma visita simples, não necessitando de registar-se.',
                            style: Theme.of(context).textTheme.bodyText1),
                        Text(
                            'Também pode criar uma visita personalizada, sendo obrigatório registar-se no website do MixMyVisit. Contudo pode adicionar os seus próprios conteúdos à visita e editá-la posteriormente.',
                            style: Theme.of(context).textTheme.bodyText1),
                      ])),
                ],
              ))
        ],
      ),
      Container(
        padding: EdgeInsets.symmetric(vertical: 6.0.h),
        child: Wrap(
            spacing: 10.0,
            runSpacing: 10.0,
            crossAxisAlignment: WrapCrossAlignment.center,
            runAlignment: WrapAlignment.spaceEvenly,
            children: [
              Container(
                  padding: EdgeInsets.only(bottom: 3.0.h),
                  child: SizedBox(
                      width: buttonSize,
                      child: ElevatedButton(
                        child: Text('Começar visita simples'),
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(
                                horizontal: 4.0.h, vertical: 3.0.h),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            )),
                        onPressed: () {
                          Dialogs.printDynamicDialog(
                              context,
                              'Criar visita simples',
                              'Tem a certeza que pretende começar uma visita simples? Está ação será irreversível, certifique-se mesmo de que deseja realizá-la.',
                              '/start-simple-visit');
                        },
                      ))),
              Container(
                  padding: EdgeInsets.only(bottom: 0.5.h),
                  child: SizedBox(
                      width: buttonSize,
                      child: ElevatedButton(
                          child: Text('Começar visita personalizada'),
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 4.0.h, vertical: 3.0.h),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              )),
                          onPressed: () {
                            Dialogs.printDynamicDialog(
                              context,
                              'Criar visita personalizada',
                              'Tem a certeza que pretende começar uma visita personalizada? Está ação será irreversível, certifique-se mesmo de que deseja realizá-la.',
                              '/start-personalized-visit',
                            );
                          })))
            ]),
      ),
      Center(
        child: TextButton.icon(
          icon: Icon(
            Icons.logout,
            color: Theme.of(context).primaryColor,
          ),
          label: Text(
            'Sair',
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
          onPressed: () {
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/home', (Route<dynamic> route) => false);
          },
        ),
      ),
    ]));
  }
}
