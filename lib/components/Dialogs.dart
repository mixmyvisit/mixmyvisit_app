import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:animations/animations.dart';

enum DialogAction { yes, abort }

class Dialogs {
  static Future<dynamic> printDynamicDialog(BuildContext context, String title,
      String body, String redirectRoute) async {
    final action = await showModal(
        useRootNavigator: true,
        configuration: FadeScaleTransitionConfiguration(
            barrierDismissible: true,
            transitionDuration: const Duration(milliseconds: 200)),
        context: context,
        builder: (context) {
          return AlertDialog(
            actionsPadding: EdgeInsets.fromLTRB(0, 0, 3.0.h, 2.0.h),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: Text(title, style: Theme.of(context).textTheme.headline3),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 2.5.h, 0, 5.0.h),
                    child: Center(
                      child: SvgPicture.asset(
                        'assets/icons/icon-error.svg',
                        semanticsLabel: 'Ícone aviso',
                        color: Colors.red[700],
                        height: 10.0.h,
                      ),
                    ),
                  ),
                  Text(
                    body,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Não'),
                onPressed: () {
                  Navigator.of(context).pop(DialogAction.abort);
                },
              ),
              ElevatedButton(
                  child: Text('Sim'),
                  onPressed: () {
                    Navigator.of(context).pop(DialogAction.yes);
                    Navigator.pushReplacementNamed(context, redirectRoute);
                  }),
            ],
          );
        });
    return (action != null) ? action : DialogAction.abort;
  }
}
