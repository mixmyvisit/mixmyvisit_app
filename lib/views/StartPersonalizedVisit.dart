import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:responsive_ui/responsive_ui.dart';

import 'package:digital_souvenir/providers/mmvServiceProvider.dart';
import 'package:digital_souvenir/api/exceptions.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:digital_souvenir/components/FeedbackScreen.dart';
import 'package:digital_souvenir/components/Loader.dart';
import 'package:digital_souvenir/providers/state.dart';

final generateAndAddPinFuture = FutureProvider.autoDispose<Map>((ref) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final mmvService = ref.read(mmvServiceProvider);

  return await mmvService.generateAndAddPin(tagId);
});

class StartPersonalizedVisit extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final getTagId = watch(globalStateProvider.state)['tagId'];

    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: MmvAppBar(),
          body: BodyBase(
              childWidget: watch(generateAndAddPinFuture).when(
                  data: (data) {
                    return FeedbackScreen(
                      duration: 40,
                      feedbackType: true,
                      hasImageFeedback: false,
                      messageWidget: [
                        Container(
                          padding: EdgeInsets.only(bottom: 4.0.h),
                          child: Text(
                              'Copie ou registe o seguinte PIN e faça scan do seguinte QRCode para começar a criar a sua visita personalizada',
                              style: Theme.of(context).textTheme.headline1),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 4.0.h),
                          child: RichText(
                            text: TextSpan(
                                text: 'Será reencaminhado(a) para ',
                                children: <TextSpan>[
                                  TextSpan(
                                      text:
                                          'o nosso bot do Facebook Messenger automaticamente após realizar o scan do QRCode. ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  TextSpan(
                                      text: 'No chat do bot necessitará de ',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1),
                                  TextSpan(
                                      text:
                                          'inserir a mensagem - PIN**** - sendo os asteríscos os números do PIN apresentado nesta tela. ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  TextSpan(
                                      text:
                                          'Esta mensagem irá permitir que comece todo o processo da criação da sua visita personalizada incluíndo o registo na plataforma MixMyVisit.',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1),
                                ],
                                style: Theme.of(context).textTheme.bodyText1),
                          ),
                        ),
                        Responsive(
                            runSpacing: 6.0.h,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              Div(
                                colS: 12,
                                colM: 5,
                                child: Center(
                                  child: QrImage(
                                    padding: EdgeInsets.zero,
                                    gapless: false,
                                    data:
                                        "http://m.me/Mixmyvisit?ref=$getTagId",
                                    version: QrVersions.auto,
                                  ),
                                ),
                              ),
                              Div(
                                colS: 12,
                                offsetM: 1,
                                colM: 6,
                                child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: data['pinList']
                                        .map<Widget>((number) => new Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 8),
                                            padding: EdgeInsets.all(15),
                                            decoration: BoxDecoration(
                                              color: Colors.grey[300],
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                            ),
                                            child: Text(number,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 18.0.sp,
                                                    color: Theme.of(context)
                                                        .primaryColor))))
                                        .toList()),
                              )
                            ]),
                      ],
                    );
                  },
                  error: (e, s) {
                    if (e is ApiRequestException) {
                      return FeedbackScreen(messageWidget: [
                        Text(e.message,
                            style: Theme.of(context).textTheme.bodyText1)
                      ], feedbackType: false);
                    } else {
                      return FeedbackScreen(messageWidget: [
                        Text(
                            'Pedimos desculpa mas ocorreu um erro no começo do processo de começar a sua visita personalizada. Tente novamente mais tarde.',
                            style: Theme.of(context).textTheme.bodyText1)
                      ], feedbackType: false);
                    }
                  },
                  loading: () => LoaderWithMessage(
                      message:
                          'Por favor aguarde enquanto verificamos a sua pulseira NFC e começamos a preparar o primeiro passo do processo de começar a sua visita personalizada'))),
        ));
  }
}
