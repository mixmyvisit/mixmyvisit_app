import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:responsive_ui/responsive_ui.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:digital_souvenir/components/Dialogs.dart';
import 'package:digital_souvenir/providers/state.dart';

class TagMenuIsAssociated extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Widget helloText;
    var tagAssociatedWithUser = watch(globalStateProvider.state)['user'];
    final String userName =
        tagAssociatedWithUser == '' ? null : tagAssociatedWithUser['username'];

    final visitType = tagAssociatedWithUser == '' ? 'simples' : 'personalizada';
    var orientation = MediaQuery.of(context).orientation;
    var width = MediaQuery.of(context).size.width;

    var buttonSize = orientation == Orientation.landscape || width > 600
        ? 270.0
        : double.infinity;

    if (tagAssociatedWithUser != '') {
      helloText = Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(vertical: 3.0.h),
          child: Text('Olá $userName',
              textAlign: TextAlign.left,
              style: Theme.of(context).textTheme.headline1));
    } else {
      helloText = Container(width: 0, height: 0);
    }

    return Center(
      child: Column(
        children: [
          Responsive(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: <Widget>[
                Div(
                  colS: 12,
                  colM: 6,
                  child: Container(
                      alignment: Alignment(0, 0),
                      padding:
                          orientation == Orientation.landscape || width > 600
                              ? EdgeInsets.only(bottom: 1.5.h)
                              : EdgeInsets.only(bottom: 2.0.h),
                      child: SvgPicture.asset(
                          'assets/illustrations/wristband-associated.svg',
                          height: 35.0.h,
                          semanticsLabel:
                              'NFC wristband is associated with a visit illustration')),
                ),
                Div(
                    colS: 12,
                    colM: 6,
                    child: Column(
                      children: [
                        helloText,
                        Text(
                            'Verificamos que a sua pulseira NFC já está associada à sua visita $visitType',
                            style: Theme.of(context).textTheme.headline1),
                        Container(
                            padding: EdgeInsets.only(top: 3.0.h),
                            child: Center(
                                child: Text(
                                    'Neste menu pode realizar várias ações como: adicionar uma localização à sua visita, verificar as localizações já adiconadas e terminar ou cancelar a visita.',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1))),
                      ],
                    )),
              ]),
          Container(
            padding: EdgeInsets.symmetric(vertical: 6.0.h),
            child: Wrap(
                spacing: 10.0,
                runSpacing: 20.0,
                crossAxisAlignment: WrapCrossAlignment.center,
                runAlignment: WrapAlignment.spaceEvenly,
                children: [
                  Container(
                      child: SizedBox(
                          width: buttonSize,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.0.h, vertical: 3.0.h),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                )),
                            child: Text('Registar Localização'),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, '/register-location');
                            },
                          ))),
                  Container(
                      child: SizedBox(
                          width: buttonSize,
                          child: ElevatedButton(
                              child: Text('Ver Localizações Registadas'),
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 4.0.h, vertical: 3.0.h),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  )),
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, '/registered-locations-info');
                              }))),
                  if (tagAssociatedWithUser != "")
                    Container(
                        child: SizedBox(
                            width: buttonSize,
                            child: ElevatedButton(
                                child: Text('Interagir com o Bot'),
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 4.0.h, vertical: 3.0.h),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    )),
                                onPressed: () {
                                  Navigator.pushNamed(
                                    context,
                                    '/redirect-to-bot',
                                  );
                                }))),
                  if (tagAssociatedWithUser != "")
                    Container(
                        child: SizedBox(
                            width: buttonSize,
                            child: ElevatedButton(
                                child: Text('Explorar Editor'),
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 4.0.h, vertical: 3.0.h),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    )),
                                onPressed: () {
                                  Navigator.pushNamed(
                                    context,
                                    '/redirect-to-editor',
                                  );
                                }))),
                  Container(
                      child: SizedBox(
                          width: buttonSize,
                          child: ElevatedButton(
                            child: Text('Terminar Visita'),
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.0.h, vertical: 3.0.h),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                )),
                            onPressed: () {
                              Navigator.pushNamed(
                                context,
                                '/end-visit',
                              );
                            },
                          ))),
                  Container(
                      child: SizedBox(
                          width: buttonSize,
                          child: ElevatedButton(
                            child: Text('Cancelar Visita'),
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.0.h, vertical: 3.0.h),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                )),
                            onPressed: () {
                              Dialogs.printDynamicDialog(
                                  context,
                                  'Cancelar Visita',
                                  'Tem a certeza que pretenda cancelar a sua visita? Esta ação é irreversivel e irá perder todo o progresso da sua visita guardado na sua pulseira e na plataforma MixMyVisit.',
                                  '/cancel-visit');
                            },
                          )))
                ]),
          ),
          Center(
            child: TextButton.icon(
              icon: Icon(
                Icons.logout,
                color: Theme.of(context).primaryColor,
              ),
              label: Text(
                'Sair',
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/home', (Route<dynamic> route) => false);
              },
            ),
          ),
        ],
      ),
    );
  }
}
