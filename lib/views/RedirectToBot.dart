import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:responsive_ui/responsive_ui.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';

class RedirectToBot extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MmvAppBar(),
      body: BodyBase(
          childWidget: Column(children: [
        Container(
          padding: EdgeInsets.only(bottom: 5.0.h),
          child: Text(
              'Faça scan do QRCode para ser redirecionado para o nosso Bot no Facebook Messenger',
              style: Theme.of(context).textTheme.headline1),
        ),
        Responsive(
            runSpacing: 4.0.h,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Div(
                colS: 12,
                colM: 5,
                child: Center(
                  child: QrImage(
                    padding: EdgeInsets.zero,
                    gapless: false,
                    data: "http://m.me/Mixmyvisit",
                    version: QrVersions.auto,
                  ),
                ),
              ),
              Div(
                colS: 12,
                offsetM: 1,
                colM: 6,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 1.0.h),
                      child: Text(
                          'No nosso Bot do Facebook Messenger poderá ver visualizar informações acerca da sua visita, terminar a mesma ou adicionar imagens ou vídeos para complementar a visita.',
                          style: Theme.of(context).textTheme.bodyText1),
                    ),
                    Text(
                        'Todas estas ações são possíveis escrevendo diferentes comandos. Caso necessite de ajuda utilize o comando - "!ajuda" - de forma a contextualizar-se com as diferentes funcionalidades do bot.',
                        style: Theme.of(context).textTheme.bodyText1),
                  ],
                ),
              ),
            ]),
        Container(
            padding: EdgeInsets.only(top: 7.0.h),
            child: Center(
                child: TextButton.icon(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Theme.of(context).primaryColor,
              ),
              label: Text('Voltar',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                  )),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ))),
      ])),
    );
  }
}
