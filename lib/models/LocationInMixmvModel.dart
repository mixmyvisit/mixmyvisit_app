class LocationInMixmvModel {
  final String name;
  final String timestamp;
  final String thumbnail;

  LocationInMixmvModel({
    this.name,
    this.timestamp,
    this.thumbnail,
  });

  factory LocationInMixmvModel.fromJson(Map<String, dynamic> json) {
    return LocationInMixmvModel(
        name: json['name'],
        timestamp: json['timestamp'],
        thumbnail: json['thumbnail']);
  }
}
