import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:responsive_ui/responsive_ui.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'package:digital_souvenir/providers/state.dart';

class TagMenuVisitRendering extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    var tagAssociatedWithUser = watch(globalStateProvider.state)['user'];
    var tagId = watch(globalStateProvider.state)['tagId'];

    final String visitType =
        tagAssociatedWithUser == '' ? 'simples' : 'personalizada';
    var orientation = MediaQuery.of(context).orientation;
    var width = MediaQuery.of(context).size.width;

    String redirectLink = tagAssociatedWithUser != ''
        ? 'https://mixmyvisit.web.ua.pt/redirect/visita/personalizada/$tagId'
        : 'https://mixmyvisit.web.ua.pt/redirect/visita/nao-personalizada/$tagId';

    return Center(
      child: Column(
        children: [
          Responsive(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: <Widget>[
                Div(
                    colS: 12,
                    colM: 6,
                    child: Container(
                        alignment: Alignment.center,
                        padding:
                            orientation == Orientation.landscape || width > 600
                                ? EdgeInsets.zero
                                : EdgeInsets.only(bottom: 6.0.h),
                        child: Center(
                            child: SizedBox(
                                width: 50.0.w,
                                height: 50.0.w,
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Theme.of(context).primaryColor),
                                ))))),
                Div(
                    colS: 12,
                    colM: 6,
                    child: Column(
                      children: [
                        Text(
                            'A visita $visitType associada a esta pulseira encontra-se no processo de render impossibilitando ações nesta pulseira',
                            style: Theme.of(context).textTheme.headline1),
                        Container(
                            padding: EdgeInsets.symmetric(vertical: 5.0.h),
                            child: Center(
                                child: Text(
                                    'Caso ainda não esteja a acompanhar o processo do render do vídeo da sua visita pode fazê-lo ao realizar scan do QRCode abaixo que irá redirecioná-lo para uma página no nosso website concebida para informá-lo(a) acerca do processo de render.',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1))),
                        QrImage(
                          padding: EdgeInsets.zero,
                          gapless: false,
                          data: redirectLink,
                          version: QrVersions.auto,
                        ),
                      ],
                    )),
              ]),
          Padding(
            padding: EdgeInsets.only(top: 8.0.h),
            child: Center(
              child: TextButton.icon(
                icon: Icon(
                  Icons.logout,
                  color: Theme.of(context).primaryColor,
                ),
                label: Text(
                  'Sair',
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed('/home');
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
