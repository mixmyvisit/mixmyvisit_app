import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 6.0.h, 0, 4.0.h),
        color: Colors.transparent,
        child: ListBody(
          mainAxis: Axis.vertical,
          children: [
            Center(
              child: Text('MixMyVisit Copyright © 2021',
                  style: TextStyle(
                      fontFamily: 'Inter',
                      color: Colors.grey[500],
                      fontSize: 10.0.sp,
                      fontWeight: FontWeight.w300)),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.5.h),
              child: Center(
                child: Text('UA - DigiMedia - AlticeLabs',
                    style: TextStyle(
                        fontFamily: 'Inter',
                        color: Colors.grey[500],
                        fontSize: 10.0.sp,
                        fontWeight: FontWeight.w300)),
              ),
            )
          ],
        ));
  }
}
