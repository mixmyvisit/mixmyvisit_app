import 'package:flutter_riverpod/flutter_riverpod.dart';

var initialState = {
  'tagId': '',
  'tagIdIsRegistered': false,
  'tagIsAssociatedWithVisit': '',
  'user': '',
  'visit': '',
  'deviceId': ''
};

final globalStateProvider =
    StateNotifierProvider((ref) => GlobalState(initialState));

class GlobalState extends StateNotifier<Map> {
  GlobalState(Map state) : super(state);

  Future<Map> setGlobalState(Map params) async {
    params.forEach((key, value) {
      state[key] = value;
    });
    state = state;
    return state;
  }

  void setTagId(tag) {
    state['tagId'] = tag;
    state = state;
  }

  void setTagIdIsRegistered(value) {
    state['tagIdIsRegistered'] = value;
    state = state;
  }

  void setDeviceId(device) {
    state['deviceId'] = device;
    state = state;
  }

  void setTagIsAssociatedWithVisit(value) {
    state['tagIsAssociatedWithVisit'] = value;
    state = state;
  }

  void resetState() {
    state = {
      'tagId': '',
      'tagIdIsRegistered': false,
      'tagIsAssociatedWithVisit': '',
      'user': '',
      'visit': '',
      'deviceId': ''
    };
    state = state;
  }
}
