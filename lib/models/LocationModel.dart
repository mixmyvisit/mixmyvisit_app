class Location {
  final String id;
  final String name;
  final String latitude;
  final String longitude;
  final String deviceId;

  Location({this.id, this.name, this.latitude, this.longitude, this.deviceId});

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      id: json['id'],
      name: json['name'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      deviceId: json['deviceId'],
    );
  }
}
