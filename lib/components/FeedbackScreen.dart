import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:responsive_ui/responsive_ui.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FeedbackScreen extends StatefulWidget {
  final bool feedbackType;
  final dynamic hasImageFeedback;
  final int duration;
  final List<Widget> messageWidget;

  @override
  FeedbackScreen({
    Key key,
    this.feedbackType,
    this.messageWidget,
    this.hasImageFeedback,
    this.duration,
  }) : super(key: key);

  State<StatefulWidget> createState() => FeedbackScreenState();
}

class FeedbackScreenState extends State<FeedbackScreen> {
  Timer timer;
  Color feedbackColors;
  bool hasImageFeedback;
  String feedbackImageLabel;
  String feedbackImage;

  @override
  void initState() {
    super.initState();
    hasImageFeedback =
        widget.hasImageFeedback != null ? widget.hasImageFeedback : true;
    feedbackColors = widget.feedbackType ? Colors.green[700] : Colors.red[700];
    feedbackImageLabel =
        widget.feedbackType ? 'Ícone de sucesso' : 'Ícone de erro';
    feedbackImage = widget.feedbackType
        ? 'assets/icons/icon-success.svg'
        : 'assets/icons/icon-error.svg';

    timer = timer = Timer(
        Duration(seconds: widget.duration != null ? widget.duration : 10), () {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: (Column(
        children: [
          Container(
              padding: EdgeInsets.only(bottom: 7.0.h),
              child: Text(
                'Está tela fechará automaticamente após alguns segundos',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey[400], fontWeight: FontWeight.w300),
              )),
          Center(
              child: hasImageFeedback == true
                  ? Responsive(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      runSpacing: 4.0.h,
                      children: [
                          Div(
                            colS: 12,
                            colM: 6,
                            child: Center(
                              child: SvgPicture.asset(
                                feedbackImage,
                                color: feedbackColors,
                                semanticsLabel: feedbackImageLabel,
                                height: 20.0.h,
                              ),
                            ),
                          ),
                          Div(
                              colS: 12,
                              colM: 6,
                              child: Column(children: widget.messageWidget))
                        ])
                  : Column(children: widget.messageWidget))
        ],
      )),
    );
  }
}
