import 'package:digital_souvenir/models/LocationInTagModel.dart';

class LocationListInTagModel {
  final List<LocationInTagModel> locations;

  LocationListInTagModel({
    this.locations,
  });

  factory LocationListInTagModel.fromJson(List<dynamic> parsedJson) {
    List<LocationInTagModel> locations = [];
    locations = parsedJson.map((i) => LocationInTagModel.fromJson(i)).toList();

    return new LocationListInTagModel(locations: locations);
  }
}
