import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:responsive_ui/responsive_ui.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/providers/state.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeState();
}

class HomeState extends State<Home> {
  bool didReadTagCorrectly;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FlutterNfcReader.read().then((onData) async {
      var _tagextId = onData.id;
      print(_tagextId);

      if (_tagextId != null || _tagextId != '' || onData.error == '') {
        context
            .read(globalStateProvider)
            .setGlobalState({
              'tagId': _tagextId,
              'tagIdIsRegistered': false,
              'tagIsAssociatedWithVisit': '',
              'user': '',
              'visit': '',
              'deviceId': ''
            })
            .then((value) => Navigator.of(context).pushNamedAndRemoveUntil(
                '/tag-menu', (Route<dynamic> route) => false))
            .catchError((error) {
              print(error);
              setState(() {
                didReadTagCorrectly = false;
              });
            });
      } else {
        setState(() {
          didReadTagCorrectly = false;
        });
      }
    });
    /*  FlutterNfcReader.read().then((onData) async {
      var _tagextId = onData.id;
      print(_tagextId);

      if (_tagextId != null || _tagextId != '' || onData.error == '') {
        context
            .read(globalStateProvider)
            .setGlobalState({
              'tagId': _tagextId,
              'tagIdIsRegistered': false,
              'tagIsAssociatedWithVisit': '',
              'user': '',
              'visit': '',
              'deviceId': ''
            })
            .then(
                (value) => Navigator.pushReplacementNamed(context, '/tag-menu'))
            .catchError((error) {
              setState(() {
                didReadTagCorrectly = false;
              });
            });
      } else {
        setState(() {
          didReadTagCorrectly = false;
        });
      }
    }); */

    return Scaffold(
      appBar: MmvAppBar(),
      body: BodyBase(
        childWidget: Responsive(
            crossAxisAlignment: WrapCrossAlignment.center,
            runSpacing: 4.0.h,
            children: [
              Div(
                colS: 12,
                colM: 5,
                child: Container(
                    alignment: Alignment(0, 0),
                    padding: EdgeInsets.only(bottom: 2.0.h),
                    child: SvgPicture.asset(
                        'assets/illustrations/scan-home.svg',
                        height: 35.0.h,
                        semanticsLabel: 'NFC wristband scan illustration')),
              ),
              Div(
                colS: 12,
                offsetM: 1,
                colM: 6,
                child: Builder(builder: (BuildContext context) {
                  if (didReadTagCorrectly == false) {
                    return ListBody(
                      children: [
                        Text(
                            'Pedimos desculpa mas ocorreu um erro a realizar o scan da sua pulseira de NFC',
                            style: Theme.of(context).textTheme.headline1),
                        Container(
                            padding: EdgeInsets.only(top: 3.0.h),
                            child: Center(
                                child: Text(
                                    'Por favor tente novamente mais tarde.',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1))),
                        Container(
                            padding: EdgeInsets.only(top: 5.0.h),
                            child: ElevatedButton(
                              onPressed: () => {
                                setState(() {
                                  didReadTagCorrectly = null;
                                })
                              },
                              child: Text('Voltar'),
                            ))
                      ],
                    );
                  } else {
                    return Column(
                      children: [
                        Text(
                            'Bem-vindo à aplicação MixMyVisit onde pode realizar visitas dinâmicas!',
                            overflow: TextOverflow.clip,
                            style: Theme.of(context).textTheme.headline1),
                        Container(
                            padding: EdgeInsets.only(top: 3.0.h),
                            child: Text(
                                'Passe a sua pulseira NFC no dispositivo para fazer o scan da mesma para começar a sua experiência.',
                                overflow: TextOverflow.clip,
                                style: Theme.of(context).textTheme.bodyText1))
                      ],
                    );
                  }
                }),
              ),
            ]),
      ),
    );
  }
}
