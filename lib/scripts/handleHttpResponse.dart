import 'package:http/http.dart' as http;

class HandleHttpResponse {
  dynamic returnResponse(http.Response response, Function functionOnSuccess) {
    switch (response.statusCode) {
      case 200:
        return functionOnSuccess();
        break;
      case 400:
        throw Exception(
            'Pedimos desculpa mas ocorreu um erro ao realizar o pedido ao servidor devido a um erro no servidor');
      case 401:
        throw Exception(
            'Pedimos desculpa mas ocorreu um erro ao realizar o pedido ao servidor devido à inexistência da informação requerida pelo pedido no servidor');
      case 403:
        throw Exception(
            'Pedimos desculpa mas ocorreu um erro ao realizar o pedido ao servidor devido a um erro no servidor');
      case 500:
        throw Exception(
            'Pedimos desculpa mas ocorreu um erro ao realizar o pedido ao servidor devido a um erro no servidor');
      default:
        throw Exception(
            'Pedimos desculpa mas ocorreu um erro no decorrer da comunicação com o servidor com o seguinte código: ${response.statusCode}');
    }
  }
}
