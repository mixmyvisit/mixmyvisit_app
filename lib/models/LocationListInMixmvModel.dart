import 'package:digital_souvenir/models/LocationInMixmvModel.dart';

class LocationListInMixmvModel {
  final List<LocationInMixmvModel> locations;

  LocationListInMixmvModel({
    this.locations,
  });

  factory LocationListInMixmvModel.fromJson(List<dynamic> parsedJson) {
    List<LocationInMixmvModel> locations = [];
    locations =
        parsedJson.map((i) => LocationInMixmvModel.fromJson(i)).toList();

    return new LocationListInMixmvModel(locations: locations);
  }
}
