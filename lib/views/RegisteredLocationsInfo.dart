import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:responsive_ui/responsive_ui.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:intl/intl.dart';

import 'package:digital_souvenir/providers/mmvServiceProvider.dart';
import 'package:digital_souvenir/api/exceptions.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:digital_souvenir/components/FeedbackScreen.dart';
import 'package:digital_souvenir/components/Loader.dart';
import 'package:digital_souvenir/models/LocationInMixmvModel.dart';
import 'package:digital_souvenir/models/LocationListInMixmvModel.dart';
import 'package:digital_souvenir/providers/state.dart';
import 'package:transparent_image/transparent_image.dart';

final getTagLocationsFuture =
    FutureProvider.autoDispose<LocationListInMixmvModel>((ref) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final mmvService = ref.read(mmvServiceProvider);

  final getVisitLocations = await mmvService.getVisitLocations(tagId);
  return getVisitLocations;
});

class RegisteredLocationsInfo extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Scaffold(
      appBar: MmvAppBar(
        showGoBack: true,
      ),
      body: BodyBase(
          childWidget: watch(getTagLocationsFuture).when(
              data: (snapshot) {
                return Column(children: [
                  Container(
                      padding: EdgeInsets.only(bottom: 6.0.h),
                      child: Text('Localizações registadas',
                          style: Theme.of(context).textTheme.headline1)),
                  Builder(builder: (
                    BuildContext context,
                  ) {
                    if (snapshot.locations.length == 0) {
                      return Responsive(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          runSpacing: 4.0.h,
                          children: [
                            Div(
                              colS: 12,
                              colM: 6,
                              child: Center(
                                child: SvgPicture.asset(
                                  'assets/icons/icon-error.svg',
                                  color: Colors.red[700],
                                  semanticsLabel: 'Ícone de erro',
                                  height: 20.0.h,
                                ),
                              ),
                            ),
                            Div(
                              colS: 12,
                              colM: 6,
                              child: Text(
                                  'Ainda não possuí qualquer tipo de localização registada nesta pulseira.',
                                  style: Theme.of(context).textTheme.bodyText1),
                            )
                          ]);
                    } else {
                      return Responsive(
                          runSpacing: 6.0.h,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: snapshot.locations
                              .map<Div>((LocationInMixmvModel locationInfo) {
                            final f = DateFormat("HH:mm:ss | dd-MM-yyyy");
                            String locationName = locationInfo.name;
                            final locationTimestamp =
                                DateTime.parse(locationInfo.timestamp);
                            final locationDate = f.format(locationTimestamp);

                            return Div(
                              colS: 12,
                              colL: 6,
                              child: Card(
                                color: Colors.grey[300],
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                          padding:
                                              EdgeInsets.only(bottom: 4.0.h),
                                          child: Center(
                                              child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                            child: Stack(
                                              children: <Widget>[
                                                Center(
                                                    child:
                                                        CircularProgressIndicator()),
                                                Center(
                                                  child:
                                                      FadeInImage.memoryNetwork(
                                                    fit: BoxFit.cover,
                                                    width: 100.0.w,
                                                    placeholder:
                                                        kTransparentImage,
                                                    image:
                                                        locationInfo.thumbnail,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ))),
                                      Container(
                                        padding: EdgeInsets.fromLTRB(
                                            3.0.h, 0, 3.0.h, 3.0.h),
                                        child: Text(
                                          '$locationName',
                                          textAlign: TextAlign.left,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline2,
                                        ),
                                      ),
                                      Container(
                                          padding: EdgeInsets.fromLTRB(
                                              3.0.h, 0, 3.0.h, 2.0.h),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 5.0),
                                                child: Text(
                                                    'Registou esta localização às:',
                                                    textAlign: TextAlign.left,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1),
                                              ),
                                              Text('$locationDate',
                                                  textAlign: TextAlign.left,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1),
                                            ],
                                          )),
                                    ]),
                              ),
                            );
                          }).toList());
                    }
                  }),
                  Container(
                      padding: EdgeInsets.only(top: 7.0.h),
                      child: Center(
                          child: TextButton.icon(
                        icon: Icon(
                          Icons.arrow_back_rounded,
                          color: Theme.of(context).primaryColor,
                        ),
                        label: Text('Voltar',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                            )),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ))),
                ]);
              },
              error: (e, s) {
                if (e is ApiRequestException) {
                  return FeedbackScreen(messageWidget: [
                    Text(
                        'Pedimos desculpa mas ocorreu um erro na obtenção das localizações guardadas na sua pulseira. Tente novamente mais tarde.',
                        style: Theme.of(context).textTheme.bodyText1)
                  ], feedbackType: false);
                } else {
                  return FeedbackScreen(messageWidget: [
                    Text("Oops, parece que algo correu mal...",
                        style: Theme.of(context).textTheme.bodyText1)
                  ], feedbackType: false);
                }
              },
              loading: () => LoaderWithMessage(
                  message:
                      'Por favor aguarde enquanto obtemos as localizações que já registou no decorrer da sua visita'))),
    );
  }
}
