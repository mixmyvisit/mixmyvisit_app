import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:digital_souvenir/providers/mmvServiceProvider.dart';
import 'package:digital_souvenir/api/exceptions.dart';

import 'package:digital_souvenir/views/TagMenuIsAssociated.dart';
import 'package:digital_souvenir/views/TagMenuIsNotAssociated.dart';
import 'package:digital_souvenir/views/TagMenuVisitRendering.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/FeedbackScreen.dart';
import 'package:digital_souvenir/components/Loader.dart';
import 'package:digital_souvenir/providers/state.dart';

final verifyTagRegistrationFuture =
    FutureProvider.autoDispose<Map<String, bool>>((ref) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final changeState = ref.read(globalStateProvider).setGlobalState;
  final mmvService = ref.read(mmvServiceProvider);

  final verifyTagRegistration =
      await mmvService.verifyTagRegistration(tagId, changeState);
  return verifyTagRegistration;
});

class TagMenu extends ConsumerWidget {
  /* Future<Map<String, bool>> verifyTagRegistration(tagId, changeState) async {
    final Uri urlVerifyTagRegistration = Uri.https(
        'mixmyvisit.web.ua.pt', '/api/mixmvs/verify-check-visit-type');

    try {
      final requestVerifyTagRegistration =
          await http.post(urlVerifyTagRegistration,
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: jsonEncode(<String, String>{
                'tagID': tagId,
              }));

      return HandleHttpResponse().returnResponse(requestVerifyTagRegistration,
          () {
        var requestData = jsonDecode(requestVerifyTagRegistration.body);

        if (requestData['visit'] != null) {
          if (requestData['user'] != null) {
            changeState({
              'tagIdIsRegistered': true,
              'tagIsAssociatedWithVisit': true,
              'user': requestData['user'],
              'visit': requestData['visit']
            });
          } else {
            changeState({
              'tagIdIsRegistered': true,
              'tagIsAssociatedWithVisit': true,
              'visit': requestData['visit']
            });
          }

          if (requestData['visit']['states_id'] == 2 ||
              requestData['visit']['states_id'] == 8 ||
              requestData['visit']['states_id'] == 14) {
            return {"isVisitRendering": true, "isAssociatedWithVisit": true};
          } else {
            return {"isVisitRendering": false, "isAssociatedWithVisit": true};
          }
        } else {
          changeState({
            'tagIdIsRegistered': false,
            'tagIsAssociatedWithVisit': false,
          });

          return {"isVisitRendering": false, "isAssociatedWithVisit": false};
        }
      });
    } on SocketException {
      throw Exception(
          'Infelizmente não foi possível realizar a operação visto que o dispositivo encontra-se sem internet');
    }
  } */

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Scaffold(
        appBar: MmvAppBar(),
        body: BodyBase(
            childWidget: watch(verifyTagRegistrationFuture).when(
                data: (data) {
                  if (!data['isAssociatedWithVisit']) {
                    return TagMenuIsNotAssociated();
                  } else {
                    if (data['isVisitRendering']) {
                      return TagMenuVisitRendering();
                    } else {
                      return TagMenuIsAssociated();
                    }
                  }
                },
                error: (e, s) {
                  if (e is ApiRequestException) {
                    return FeedbackScreen(messageWidget: [
                      Text(e.message,
                          style: Theme.of(context).textTheme.bodyText1)
                    ], feedbackType: false);
                  } else {
                    return FeedbackScreen(messageWidget: [
                      Text(
                          'Pedimos desculpa mas ocorreu um erro ao verificar se a sua pulseira já está registada ou se está associada com alguma visita. Tente novamente mais tarde.',
                          style: Theme.of(context).textTheme.bodyText1)
                    ], feedbackType: false);
                  }
                },
                loading: () => LoaderWithMessage(
                    message:
                        'Por favor aguarde enquanto verificamos a sua pulseira NFC'))));
  }
}
