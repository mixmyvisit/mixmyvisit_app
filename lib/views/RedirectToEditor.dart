import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:responsive_ui/responsive_ui.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:digital_souvenir/providers/state.dart';

class RedirectToEditor extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final tagId = watch(globalStateProvider.state)['tagId'];

    return Scaffold(
      appBar: MmvAppBar(),
      body: BodyBase(
          childWidget: Column(children: [
        Container(
          padding: EdgeInsets.only(bottom: 5.0.h),
          child: Text(
              'Faça scan do QRCode para ser redirecionado para o editor da sua visita',
              style: Theme.of(context).textTheme.headline1),
        ),
        Responsive(
            runSpacing: 4.0.h,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Div(
                colS: 12,
                colM: 5,
                child: Center(
                  child: QrImage(
                    padding: EdgeInsets.zero,
                    gapless: false,
                    data:
                        "https://mixmyvisit.web.ua.pt/redirect/visita/personalizada/$tagId?to=editor",
                    version: QrVersions.auto,
                  ),
                ),
              ),
              Div(
                colS: 12,
                offsetM: 1,
                colM: 6,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 1.0.h),
                      child: Text(
                          'No editor da sua visita, disponível no website do MixMyVisit pode adicionar, eliminar e reordenar conteúdos na sua visita. Ainda é possibilitada a adição de um comentário nos conteúdos da visita.',
                          style: Theme.of(context).textTheme.bodyText1),
                    ),
                    Text(
                        'Para além disso, também é possível terminar a sua visita via editor ao carregar no botão - "Renderizar".',
                        style: Theme.of(context).textTheme.bodyText1),
                  ],
                ),
              ),
            ]),
        Container(
            padding: EdgeInsets.only(top: 7.0.h),
            child: Center(
                child: TextButton.icon(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Theme.of(context).primaryColor,
              ),
              label: Text('Voltar',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                  )),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ))),
      ])),
    );
  }
}
