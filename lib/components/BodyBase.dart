import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:digital_souvenir/components/Footer.dart';

class BodyBase extends StatelessWidget {
  BodyBase({this.childWidget});

  final Widget childWidget;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
              maxHeight: double.infinity,
              maxWidth: MediaQuery.of(context).size.width,
            ),
            child: ListView(
                padding: EdgeInsets.fromLTRB(5.0.h, 5.0.h, 5.0.h, 0),
                shrinkWrap: true,
                children: [
                  Container(
                      constraints: BoxConstraints(
                          minHeight:
                              SizerUtil.orientation == Orientation.landscape &&
                                      MediaQuery.of(context).size.width > 600.0
                                  ? 30.0
                                  : 60.0.h,
                          maxWidth: 100.0.w),
                      child: childWidget),
                  Footer(),
                ])));
  }
}
