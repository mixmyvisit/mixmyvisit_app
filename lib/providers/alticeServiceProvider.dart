import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:digital_souvenir/api/alticeService.dart';

final alticeServiceProvider = Provider<AlticeService>((ref) {
  return AlticeService(Dio());
});
