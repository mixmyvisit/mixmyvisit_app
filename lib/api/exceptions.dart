import 'package:dio/dio.dart';

class ApiRequestException implements Exception {
  ApiRequestException.fromDioError(DioError dioError) {
    switch (dioError.type) {
      case DioErrorType.response:
        message = _handleError(dioError.response.statusCode);
        break;
      case DioErrorType.cancel:
        message = "O pedido à API foi cancelado, tente novamente mais tarde";
        break;
      case DioErrorType.connectTimeout:
        message =
            "Pedimos desculpa mas infelizmente não foi possível realizar a comunicação com a API visto que o dispositivo encontra-se sem internet";
        break;
      case DioErrorType.other:
        message =
            "Pedimos desculpa mas infelizmente não foi possível realizar a comunicação com a API visto que o dispositivo encontra-se sem internet";
        break;
      case DioErrorType.receiveTimeout:
        message =
            "Pedimos desculpa mas o tempo de comunicação para com a API ultrapassou o limite, tente novamente mais tarde";
        break;
      case DioErrorType.sendTimeout:
        message =
            "Pedimos desculpa mas o tempo de comunicação para com a API ultrapassou o limite, tente novamente mais tarde";
        break;
      default:
        message =
            "Pedimos desculpa mas algo correu mal na comunicação com a API";
        break;
    }
  }

  String message;

  String _handleError(int statusCode) {
    switch (statusCode) {
      case 400:
        return 'Pedimos desculpa mas algo correu mal no pedido realizada à API';
      case 403:
        return 'Pedimos desculpa mas excedeu o limite de conteúdos que pode adicionar à sua visita. Para conseguir adicionar novamente uma localização necessita de eliminar um desses conteúdos a partir do editor da sua visita do website do MixMyVisit. Caso esteja a realizar uma visita simples não conseguirá adicionar mais nenhum conteúdo à sua visita.';
      case 404:
        return 'Pedimos desculpa mas a informação pedida à API não existe';
      case 500:
        return 'Pedimos desculpa mas um erro ocorreu na API';
      default:
        return 'Pedimos desculpa mas algo correu mal na comunicação com a API';
    }
  }

  @override
  String toString() => message;
}
