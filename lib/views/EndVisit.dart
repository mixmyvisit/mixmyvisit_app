import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:responsive_ui/responsive_ui.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:animations/animations.dart';

import 'package:digital_souvenir/providers/mmvServiceProvider.dart';
import 'package:digital_souvenir/api/exceptions.dart';

import 'package:digital_souvenir/components/BodyBase.dart';
import 'package:digital_souvenir/components/MmvAppBar.dart';
import 'package:digital_souvenir/components/FeedbackScreen.dart';
import 'package:digital_souvenir/components/Loader.dart';
import 'package:digital_souvenir/providers/state.dart';

final startEndProcessOfNonPersVisitFuture = FutureProvider.autoDispose
    .family<bool, String>((ref, locationsLength) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final mmvService = ref.read(mmvServiceProvider);
  await mmvService.startEndProcessOfNonPersVisit(tagId, locationsLength);
  return await mmvService.createTokenToEndVisit(tagId);
});

final startEndProcessOfPersVisitFuture = FutureProvider.autoDispose
    .family<bool, String>((ref, locationsLength) async {
  final tagId = ref.read(globalStateProvider.state)['tagId'];
  final userId = ref.read(globalStateProvider.state)['user']['id'];
  final mmvService = ref.read(mmvServiceProvider);
  return await mmvService.startEndProcessOfPersVisit(
      tagId, userId, locationsLength);
});

class EndVisit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EndVisitState();
}

class EndVisitState extends State<EndVisit> {
  String locationVideosLength = '';
  bool didChooseLocationVideosLength = false;

  Future<dynamic> printDialog(BuildContext context) async {
    final title = locationVideosLength != ''
        ? 'Terminar Visita'
        : 'Escolha a duração dos vídeos da sua visita';
    final body = locationVideosLength != ''
        ? 'Tem a certeza que prentede terminar a sua visita? Esta ação é irreversível e não poderá adicionar mais nenhuma localização à sua visita.'
        : 'De forma a ser possível enviar a sua visita, constituída por todas os vídeos das localizações registadas, é necessário escolher obrigatoriamente um tipo de duração para os vídeos das localizações que fazem parte da visita.';
    final dialogActions = locationVideosLength != ''
        ? <Widget>[
            TextButton(
              child: Text('Não'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            ElevatedButton(
                child: Text('Sim'),
                onPressed: () {
                  Navigator.of(context).pop();
                  if (locationVideosLength != '') {
                    setState(() {
                      didChooseLocationVideosLength = true;
                    });
                  }
                }),
          ]
        : <Widget>[
            TextButton(
              child: Text('Voltar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ];

    await showModal(
        useRootNavigator: true,
        configuration: FadeScaleTransitionConfiguration(
            barrierDismissible: true,
            transitionDuration: const Duration(milliseconds: 200)),
        context: context,
        builder: (context) {
          return AlertDialog(
              actionsPadding: EdgeInsets.fromLTRB(0, 0, 3.0.h, 2.0.h),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: Text(title, style: Theme.of(context).textTheme.headline3),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 2.5.h, 0, 5.0.h),
                      child: Center(
                        child: SvgPicture.asset(
                          'assets/icons/icon-error.svg',
                          semanticsLabel: 'Ícone aviso',
                          color: Colors.red[700],
                          height: 10.0.h,
                        ),
                      ),
                    ),
                    Text(
                      body,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
              ),
              actions: dialogActions);
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final tagIdUser = context.read(globalStateProvider.state)['user'];
    final tagId = context.read(globalStateProvider.state)['tagId'];

    String redirectLink = tagIdUser != ''
        ? 'https://mixmyvisit.web.ua.pt/redirect/visita/personalizada/$tagId?to=visit'
        : 'https://mixmyvisit.web.ua.pt/redirect/visita/nao-personalizada/$tagId';
    final String visitType = tagIdUser != '' ? 'personalizada' : 'simples';
    final String finalIntructions = tagIdUser != ''
        ? 'Após o processo de renderização da visita estar concluído será notificado(a) via email e Facebook Messenger, e será possibilitada a ação de fazer download da visita e também de partilhá-la nas diferentes redes sociais ao aceder à página da sua visita via o nosso website.'
        : 'Só após a sua visita estar "renderizada" será possibilitada a ação de fazer download da mesma e também de partilhá-la nas diferentes redes sociais.';

    return WillPopScope(
        onWillPop: () async => didChooseLocationVideosLength == false,
        child: Scaffold(
          appBar: MmvAppBar(showGoBack: didChooseLocationVideosLength == false),
          body: BodyBase(childWidget: Builder(builder: (BuildContext context) {
            if (didChooseLocationVideosLength == false) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(bottom: 4.0.h),
                      child: Text(
                          'Selecione a duração dos vídeos das localizações registadas',
                          style: Theme.of(context).textTheme.headline1)),
                  Container(
                      padding: EdgeInsets.only(bottom: 1.0.h),
                      child: Text(
                          'Antes de realizar qualquer ação relativa ao processo de finalização da visita é necessário que escolha a duração, curta ou longa, dos vídeos das localizações que registou durante a visita. Pode escolher abaixo a sua preferência.',
                          style: Theme.of(context).textTheme.bodyText1)),
                  Container(
                      padding: EdgeInsets.only(bottom: 3.0.h),
                      child: Text(
                          'É de referir que caso escolha a opção de vídeos das localizações de durações mais longas a sua visita irá demorar mais tempo até ficar finalizada.',
                          style: Theme.of(context).textTheme.bodyText1)),
                  ListTile(
                    title: Text('Vídeos de localizações curtos',
                        style: Theme.of(context).textTheme.bodyText1),
                    leading: Radio(
                      value: 'small',
                      groupValue: locationVideosLength,
                      onChanged: (value) {
                        setState(() {
                          locationVideosLength = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: Text('Vídeos de localizações longos',
                        style: Theme.of(context).textTheme.bodyText1),
                    leading: Radio(
                      value: 'long',
                      groupValue: locationVideosLength,
                      onChanged: (value) {
                        setState(() {
                          locationVideosLength = value;
                        });
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 4.0.h),
                    child: Center(
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.0.h, vertical: 3.0.h),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                )),
                            onPressed: () => printDialog(context),
                            child: Text('Avançar e terminar visita'))),
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 5.0.h),
                      child: Center(
                          child: TextButton.icon(
                        icon: Icon(
                          Icons.arrow_back_rounded,
                          color: Theme.of(context).primaryColor,
                        ),
                        label: Text('Cancelar e voltar',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                            )),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ))),
                ],
              );
            } else {
              return Consumer(builder: (context, watch, child) {
                var futureToWatch = tagIdUser != ''
                    ? startEndProcessOfPersVisitFuture(locationVideosLength)
                    : startEndProcessOfNonPersVisitFuture(locationVideosLength);

                return watch(futureToWatch).when(
                    data: (_) {
                      return FeedbackScreen(
                        duration: 40,
                        feedbackType: true,
                        messageWidget: [
                          Container(
                            padding: EdgeInsets.only(bottom: 4.0.h),
                            child: Text(
                                'A sua visita $visitType está agora a "renderizar"! Faça scan do QRCode para acompanhar este processo da sua visita no nosso website.',
                                style: Theme.of(context).textTheme.headline1),
                          ),
                          Center(
                            child: Responsive(
                                runSpacing: 4.0.h,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  Div(
                                    colS: 12,
                                    colM: 5,
                                    child: Center(
                                      child: QrImage(
                                        padding: EdgeInsets.zero,
                                        gapless: false,
                                        data: redirectLink,
                                        version: QrVersions.auto,
                                      ),
                                    ),
                                  ),
                                  Div(
                                    colS: 12,
                                    offsetM: 1,
                                    colM: 6,
                                    child: Column(children: [
                                      Container(
                                          padding:
                                              EdgeInsets.only(bottom: 0.5.h),
                                          child: Text(
                                              'Na página para qual será redirecionado(a), após a leitura do QRCode, conseguirá determinar se a sua visita terminou ou não processo de "renderização". Esta pagina será atualizada automaticamente de 2 em 2 minutos.',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1)),
                                      Container(
                                        padding: EdgeInsets.only(bottom: 0.2.h),
                                        child: Text(
                                            'Este processo processo de "render" do vídeo da sua visita poderá demorar vários minutos até estar concluído, dependendo dos conteúdos adicionados à visita ou da escolha de vídeos de localizações longos ou curtos.',
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                color: Color(0xFF191825),
                                                fontFamily: 'Inter',
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Text(finalIntructions)
                                    ]),
                                  )
                                ]),
                          ),
                        ],
                      );
                    },
                    error: (e, s) {
                      if (e is ApiRequestException) {
                        return FeedbackScreen(messageWidget: [
                          Text(e.message,
                              style: Theme.of(context).textTheme.bodyText1)
                        ], feedbackType: false);
                      } else {
                        return FeedbackScreen(messageWidget: [
                          Text(
                              'Pedimos desculpa mas ocorreu um erro ao começar o processo de terminar a sua visita. Tente novamente mais tarde.',
                              style: Theme.of(context).textTheme.bodyText1)
                        ], feedbackType: false);
                      }
                    },
                    loading: () => LoaderWithMessage(
                        message:
                            'Por favor aguarde só um momento enquanto preparamos o primeiro passo do processo para terminar a sua visita'));
              });
            }
          })),
        ));
  }
}
