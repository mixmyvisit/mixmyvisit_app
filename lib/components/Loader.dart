import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class LoaderWithMessage extends StatelessWidget {
  LoaderWithMessage({Key key, this.message}) : super(key: key);
  final String message;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: [
        Container(
            padding: EdgeInsets.fromLTRB(0, 2.0.h, 0, 6.0.h),
            child: SizedBox(
                width: 50.0.w,
                height: 50.0.w,
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(
                      Theme.of(context).primaryColor),
                ))),
        Text(message,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline3)
      ],
    ));
  }
}
