import 'package:digital_souvenir/models/LocationModel.dart';

class LocationList {
  final List<Location> locations;

  LocationList({
    this.locations,
  });

  factory LocationList.fromJson(List<dynamic> parsedJson) {
    List<Location> locations = [];
    locations = parsedJson.map((i) => Location.fromJson(i)).toList();

    return new LocationList(locations: locations);
  }
}
